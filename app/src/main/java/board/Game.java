package board;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import android.util.Log;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;


import androidx.appcompat.app.AppCompatActivity;

import board.*;
import Pieces.*;

public class Game implements Serializable {

    private String gameTitle;

    private ArrayList<String> originMoveList;
    private ArrayList<String> destinationMoveList;
    private LocalDate date;
    private ArrayList<PromotedPiece> promotedPieces = new ArrayList<>();
    private String endType;


    public Game(String title, ArrayList<String> originList, ArrayList<String> destinationList, LocalDate d, ArrayList<PromotedPiece> pPieces, String type){
        gameTitle = title;
        originMoveList = originList;
        destinationMoveList = destinationList;
        date = d;
        promotedPieces = pPieces;
        endType = type;

    }


    public String getTitle(){
        return this.gameTitle;
    }

    public ArrayList<String> getOriginMoveList(){

        return this.originMoveList;

    }

    public ArrayList<String> getDestinationMoveList(){

        return this.destinationMoveList;

    }


    public LocalDate getDate(){

        return this.date;

    }

    public ArrayList<PromotedPiece> getPromotedPieces(){
        return this.promotedPieces;
    }

    public ChessPiece getPromotedPiece(int count){
        for(PromotedPiece promotedPiece : promotedPieces){
            if(promotedPiece.getMoveCount() == count){
                return promotedPiece.getPromotedPiece();
            }
        }

        return null;
    }

    public String getEndType(){
        return this.endType;
    }









}
