package board;

import java.io.Serializable;

import Pieces.ChessPiece;

public class PromotedPiece implements Serializable {

    private int moveCount;
    private ChessPiece promotedPiece;

    public PromotedPiece(int count, ChessPiece piece){
        moveCount = count;
        promotedPiece = piece;
    }


    public int getMoveCount(){
        return this.moveCount;
    }
    public ChessPiece getPromotedPiece(){
        return this.promotedPiece;
    }


}
