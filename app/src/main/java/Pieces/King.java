/**
 * 
 * 
 * @author il166, yuc1
 * 
 * 
 * 
 */

package Pieces;

import java.util.ArrayList;
import java.util.Arrays;

import board.Board;

public class King extends ChessPiece{
	
	public int moveCounter = 0;
	
	public King(char color, int row, int column){
		super(row,column, color);

	}

	/**
	 * ToStrings the object King
	 * @return String of the object
	 */
	public String toString() {
		return super.getColor()+"K";
		
	}

	/**
	 * Gets a list of all the possible moves that king can make. 
	 * @return ArrayList<int[]> of all possible moves that piece can do. 
	 */
	@Override
	public ArrayList<int[]> validMoves() {
		
		int[] initialPos = super.getPosition();

		ArrayList<int[]> listOfMoves = new ArrayList<>();
		
		int row= initialPos[0];
		int column=initialPos[1];
		ChessPiece piece;
		
		
		//all down checks
		if(row!=7) {
			//check down
			piece = Board.getPieceAtLocation(new int[] {row+1, column});
			if(piece==null || piece.getColor()!=super.getColor())
				listOfMoves.add(new int[] {row+1, column});
			
			//check down right
			if(column!=7) {
				piece = Board.getPieceAtLocation(new int[] {row+1, column+1});
				if(piece==null || piece.getColor()!=super.getColor())
					listOfMoves.add(new int[] {row+1, column+1});
			}
			
			if(column!=0) {
				//check down left
				piece = Board.getPieceAtLocation(new int[] {row+1, column-1});
				if(piece==null || piece.getColor()!=super.getColor())
					listOfMoves.add(new int[] {row+1, column-1});
			}
		}

		//all right checks
		if(column!=7) {
			
			//check right
			piece = Board.getPieceAtLocation(new int[] {row, column+1});
			if(piece==null || piece.getColor()!=super.getColor())
				listOfMoves.add(new int[] {row, column+1});
			
			if(row!=0) {
				//check upright
				piece = Board.getPieceAtLocation(new int[] {row-1, column+1});
				if(piece==null || piece.getColor()!=super.getColor())
					listOfMoves.add(new int[] {row-1, column+1});
			}
		}

		
		// all left checks
		if(column!=0) {
			
			//check left
			piece = Board.getPieceAtLocation(new int[] {row, column-1});
			if(piece==null || piece.getColor()!=super.getColor())
				listOfMoves.add(new int[] {row, column-1});

			if(row!=0) {
			//check up left
			piece = Board.getPieceAtLocation(new int[] {row-1, column-1});
			if(piece==null || piece.getColor()!=super.getColor())
				listOfMoves.add(new int[] {row-1, column-1});
			}
		}
		
		//all up checks
		if(row!=0) {
		//check up
		piece = Board.getPieceAtLocation(new int[] {row-1, column});
		if(piece==null || piece.getColor()!=super.getColor())
			listOfMoves.add(new int[] {row-1, column});
		}
		
		
		
		
		
		
	
	

		
		return listOfMoves;
	}
	
}
