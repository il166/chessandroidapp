package com.example.chess;

import android.os.Bundle;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import android.util.Log;
import java.time.*;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Hashtable;

import board.*;
import Pieces.*;

public class ReplayGame  extends AppCompatActivity {

    protected Board board;
    private int moveCount;
    private String selectedGameString;
    public ArrayList<Game> gameList = new ArrayList<>();
    private Game selectedGame;
    boolean sortByDateBoolean;
    boolean putBack = false;
    boolean previous;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay_game);
        moveCount = 0;
        board = new Board();
        setUpButtons();
        boolean availableGames = false;
        try {
            readGames();
        }
        catch(IOException io){
            io.printStackTrace();
        }
        catch(ClassNotFoundException c){
            c.printStackTrace();
        }
        //gameList = MainActivity.gameList;



        if(gameList.size() == 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Games for Replay");
            builder.setMessage("There are no availble games for replay. Please play a game and save it to replay a game.");


            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(mainIntent);

                }
            });


            builder.create().show();
        }
        else {
            availableGames = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog dialog = builder.create();

            builder.setTitle("Choose Game Replay");




            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Sort by Date", new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface d, int id){
                    sortByDateBoolean = true;
                    String[] sortedList = sortByDate();
                    new AlertDialog.Builder(ReplayGame.this).setTitle("Choose Game Replay").setItems(sortedList, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedGameString = sortedList[which];
                            System.out.println("selectedGameString: " + selectedGameString);
                            for (Game g : gameList) {
                                System.out.println("game title: " + g.getTitle());
                                String[] gameTitleAndDate = selectedGameString.split("~");
                                if (g.getTitle().equals(gameTitleAndDate[0].trim())) {
                                    selectedGame = g;
                                    break;
                                }
                            }


                        }
                    })
                    .show();




                }
            });
            dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Sort by Title", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface d, int id){
                    sortByDateBoolean = false;
                    String[] sortedList = sortByTitle();
                    new AlertDialog.Builder(ReplayGame.this).setTitle("Choose Game Replay").setItems(sortedList, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedGameString = sortedList[which];
                            System.out.println("selectedGameString: " + selectedGameString);
                            for (Game g : gameList) {
                                System.out.println("game title: " + g.getTitle());
                                String[] gameTitleAndDate = selectedGameString.split("~");
                                if (g.getTitle().equals(gameTitleAndDate[0].trim())) {
                                    selectedGame = g;

                                    break;
                                }
                            }


                        }
                    }).show();



                }
            });

            dialog.show();














        }





    }


    public void setUpButtons() {
        LinearLayout layout0 = findViewById(R.id.layout_0);
        LinearLayout layout1 = findViewById(R.id.layout_1);
        LinearLayout layout2 = findViewById(R.id.layout_2);
        LinearLayout layout3 = findViewById(R.id.layout_3);
        LinearLayout layout4 = findViewById(R.id.layout_4);
        LinearLayout layout5 = findViewById(R.id.layout_5);
        LinearLayout layout6 = findViewById(R.id.layout_6);
        LinearLayout layout7 = findViewById(R.id.layout_7);

        for (int i = 0; i < layout0.getChildCount(); i++) {
            View v = layout0.getChildAt(i);
        }

        for (int i = 0; i < layout1.getChildCount(); i++) {
            View v = layout1.getChildAt(i);
        }

        for (int i = 0; i < layout2.getChildCount(); i++) {
            View v = layout2.getChildAt(i);
        }

        for (int i = 0; i < layout3.getChildCount(); i++) {
            View v = layout3.getChildAt(i);
        }

        for (int i = 0; i < layout4.getChildCount(); i++) {
            View v = layout4.getChildAt(i);
        }

        for (int i = 0; i < layout5.getChildCount(); i++) {
            View v = layout5.getChildAt(i);
        }

        for (int i = 0; i < layout6.getChildCount(); i++) {
            View v = layout6.getChildAt(i);
        }

        for (int i = 0; i < layout7.getChildCount(); i++) {
            View v = layout7.getChildAt(i);
        }

    }





    public void nextMove(View view){
        if(selectedGame == null){
            Toast.makeText(this, "No game selected.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(moveCount == selectedGame.getOriginMoveList().size()){
            System.out.println(selectedGame.getEndType());
            if(selectedGame.getEndType().equals("draw")){
                Toast.makeText(this, "Game ended in a draw!.", Toast.LENGTH_SHORT).show();
            }
            else if(selectedGame.getEndType().equals("resign")){
                Toast.makeText(this, "Game ended in a resign!.", Toast.LENGTH_SHORT).show();
            }
            else if(selectedGame.getEndType().equals("checkmateWhite")){
                Toast.makeText(this, "Checkmate! White wins!.", Toast.LENGTH_SHORT).show();
            }
            else if(selectedGame.getEndType().equals("checkmateBlack")){
                Toast.makeText(this, "Checkmate! Black wins!.", Toast.LENGTH_SHORT).show();
            }


        }
        else {
            String origin = selectedGame.getOriginMoveList().get(moveCount);
            String destination = selectedGame.getDestinationMoveList().get(moveCount);
            replayMovePiece(origin, destination, false);
            moveCount++;

        }
    }


    public void previousMove(View view){
        if(selectedGame == null){
            Toast.makeText(this, "No game selected.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(moveCount == 0){
            Toast.makeText(this, "No Previous Moves", Toast.LENGTH_SHORT).show();

        }
        else {
            moveCount--;
            board.moveCounter-=2;
            String origin = selectedGame.getOriginMoveList().get(moveCount);
            String destination = selectedGame.getDestinationMoveList().get(moveCount);
            replayMovePiece(destination, origin, true);


        }

    }


    public void replayMovePiece(String origin, String destination, boolean previous){
        boolean promote = false;
        //System.out.println("moveCount: " + moveCount);
        //System.out.println("Move Counter: " + board.moveCounter);
        //System.out.println("origin: " + origin);
        //System.out.println("destination: " + destination);
        ChessPiece movingPiece = Board.getPieceAtLocation(Board.translateToArray(origin));
        //System.out.println("PROMOTED HASH SIZE: " + board.promotedPieces.size());
        //System.out.println("REMovED PIECES HASH SIZE: " + board.table.size());
        if(board.table.get(moveCount) != null && previous){
            System.out.println("IN REMOVE PIECE CASE ");
            putBack = true;
        }
        String pieceName = movingPiece.toString();
        System.out.println(pieceName);
        int firstButtonId = getResources().getIdentifier("button_" + origin, "id", getPackageName());
        ImageButton firstButton = ((ImageButton) findViewById(firstButtonId));

        int secondButtonID = getResources().getIdentifier("button_" + destination, "id", getPackageName());
        ImageButton secondButton = ((ImageButton) findViewById(secondButtonID));


        Resources res = getResources();
        int movingPieceID = res.getIdentifier(pieceName.toLowerCase(), "drawable", getPackageName());
        if(selectedGame.getPromotedPiece(moveCount) != null){
            System.out.println("PROMOTION IN REPLAY");
            ChessPiece promotedPiece = selectedGame.getPromotedPiece(moveCount);
            movingPieceID = res.getIdentifier(promotedPiece.toString().toLowerCase(), "drawable", getPackageName());
            int dest[] = {Integer.parseInt(origin.substring(0, 1)), Integer.parseInt((origin.substring(1)))};
            if(promotedPiece.getColor() == 'w')
                board.board[dest[0]][dest[1]] = promotedPiece;
            else{
                board.board[dest[0]][dest[1]] = promotedPiece;
            }


            if(putBack && previous){
                if(promotedPiece.getColor() == 'w')
                    board.board[dest[0]][dest[1]] = new Pawn('w', dest[0], dest[1]);
                else{
                    board.board[dest[0]][dest[1]] = new Pawn('b', dest[0], dest[1]);
                }
                promote = true;

            }
        }
        secondButton.setImageResource(movingPieceID);

        firstButton.setImageResource(android.R.color.transparent);


        //if promotion in replay
        board.movePieceReplay(origin,destination);
        if(putBack && previous){
            ChessPiece newPiece = board.table.get(moveCount);
            System.out.println("putting back: " + newPiece);
            board.table.remove(moveCount);
            int dest[] = {Integer.parseInt(origin.substring(0, 1)), Integer.parseInt((origin.substring(1)))};

            System.out.println(Integer.toString(dest[0]) + Integer.toString(dest[1]));
            board.board[dest[0]][dest[1]] = newPiece;
            board.printBoard(board.board);
            int moveID = res.getIdentifier(newPiece.toString().toLowerCase(), "drawable", getPackageName());
            firstButton.setImageResource(moveID);

            if(promote){
                if(newPiece.getColor() == 'w') {
                    moveID = res.getIdentifier("bp", "drawable", getPackageName());
                    secondButton.setImageResource(moveID);
                }
                else{
                    moveID = res.getIdentifier("wp", "drawable", getPackageName());
                    secondButton.setImageResource(moveID);
                }
            }

            putBack = false;
        }



    }








    /**
     * reads user from the Users.ser file with serializable
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void readGames() throws IOException, ClassNotFoundException{
        Log.e("====== READING REPLAY ==========>", "READING IN REPLAY");

        try {


            FileInputStream fileInputStream = this.openFileInput("Games.ser");
            //System.out.println("read");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            gameList = (ArrayList<Game>)objectInputStream.readObject();
            //Log.e("ARRAYLIST MADE", "GAMELIST SHOULDNT BE FUCKING EMPTY");
            objectInputStream.close();
            fileInputStream.close();

        }
        //nothing in file
        catch(FileNotFoundException f){
            System.out.println("file not found, making new gameList");
            gameList = new ArrayList<Game>();
        }
        catch(EOFException e) {
            Log.e("MAKE", "MAKING ARRAYLIST");
            gameList = new ArrayList<Game>();

        }

    }


    public String[] sortByTitle(){
        String[] glistArray = new String[gameList.size()];
        for (int i = 0; i < gameList.size(); i++) {
            //System.out.println(i);
            glistArray[i] = gameList.get(i).getTitle() + " ~ "  + gameList.get(i).getDate();
            //System.out.println(glistArray[i]);
        }

        Arrays.sort(glistArray);


        return glistArray;
    }


    public String[] sortByDate(){
        String[] glistArray = new String[gameList.size()];
        ArrayList<LocalDate> dates = new ArrayList<>();
        for(Game g : gameList){
            dates.add(g.getDate());
        }
        ArrayList<Game> gameListCopy = gameList;
        gameListCopy.sort(new Comparator<Game>() {
            @Override
            public int compare(Game game, Game t1) {
                return game.getDate().compareTo(t1.getDate());
            }
        });


        for(int i = 0; i < gameListCopy.size(); i++){
            glistArray[i] = gameListCopy.get(i).getTitle() + " ~ "  + gameListCopy.get(i).getDate();
        }



        return glistArray;
    }











}
