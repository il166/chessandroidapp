package com.example.chess;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.*;
import android.widget.*;
import android.util.Log;
import android.util.Pair;
import java.time.LocalDate;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;

import board.*;
import Pieces.*;


public class NewGame extends AppCompatActivity implements View.OnClickListener, Serializable {

    protected boolean firstClick = false;
    protected boolean secondClick = false;
    protected String firstTile = "";
    protected String secondTile = "";
    protected boolean whitesTurn = true;
    protected Board board;
    static ArrayList<Game> gameList = new ArrayList<Game>();
    protected ArrayList<String> originMoveList = new ArrayList<String>();
    protected ArrayList<String> destinationMoveList = new ArrayList<String>();
    boolean saveGame = false;
    private ArrayList<Integer> AIMoves = new ArrayList<Integer>();
    private int moveCount;
    boolean undoDone = false;
    boolean resign;
    boolean draw;
    boolean checkmateWhite;
    boolean checkmateBlack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
        gameList = MainActivity.gameList;
        board = new Board();
        moveCount = 0;
        board.moveCounter = 0;
        boolean resign = false;
        boolean draw = false;
        boolean checkmateWhite = false;
        boolean checkmateBlack = false;
        setUpButtons();
    }

    public void setUpButtons() {
        LinearLayout layout0 = findViewById(R.id.layout_0);
        LinearLayout layout1 = findViewById(R.id.layout_1);
        LinearLayout layout2 = findViewById(R.id.layout_2);
        LinearLayout layout3 = findViewById(R.id.layout_3);
        LinearLayout layout4 = findViewById(R.id.layout_4);
        LinearLayout layout5 = findViewById(R.id.layout_5);
        LinearLayout layout6 = findViewById(R.id.layout_6);
        LinearLayout layout7 = findViewById(R.id.layout_7);
        RelativeLayout relLayout1 = findViewById(R.id.relLayout_0);
        Button AI_Button = findViewById(R.id.AI);
        Button undoButton = findViewById(R.id.undo);
        Button resignButton = findViewById(R.id.resign_button);
        Button drawButton = findViewById(R.id.draw_button);


        AI_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<Integer> move = Board.getAValidMove(whitesTurn);
                AIMoves = move;
                if (move == null)
                    return;

                Log.d("dest", " : " + move.get(0));
                int firstButtonId;
                if (move.get(0) < 10) {
                    firstButtonId = getResources().getIdentifier("button_0" + move.get(0), "id", getPackageName());

                } else {
                    firstButtonId = getResources().getIdentifier("button_" + move.get(0), "id", getPackageName());
                }

                ImageButton firstButton = ((ImageButton) findViewById(firstButtonId));
                int secondButtonID;

                if (move.get(1) < 10) {
                    secondButtonID = getResources().getIdentifier("button_0" + move.get(1), "id", getPackageName());

                } else {
                    secondButtonID = getResources().getIdentifier("button_" + move.get(1), "id", getPackageName());
                }
                ImageButton secondButton = ((ImageButton) findViewById(secondButtonID));

                firstButton.setBackgroundColor(Color.parseColor("#32A6A8"));

                secondButton.setBackgroundColor(Color.parseColor("#A83232"));

            }
        });


        undoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //System.out.println("MOVE COUNTER: " + board.moveCounter);
                //System.out.println("MoVE COUNT: " + moveCount);
                board.moveCounter -= 2;
                moveCount--;
                if (originMoveList.size() == 0) {
                    Toast.makeText(NewGame.this, "No moves to undo.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (undoDone) {
                    Toast.makeText(NewGame.this, "Last move was undone; Cannot undo this move.", Toast.LENGTH_SHORT).show();
                    return;
                }

                boolean putBack = false;
                boolean promote = false;
                ChessPiece movingPiece = Board.getPieceAtLocation(Board.translateToArray(destinationMoveList.get(destinationMoveList.size() - 1)));
                //System.out.println("HASH SIZE: " + board.table.size());
                if (board.table.get(moveCount) != null) {
                    System.out.println("IN REMOVE PIECE CASE UNDO");
                    putBack = true;

                }
                String pieceName = movingPiece.toString();
                System.out.println(pieceName);
                int firstButtonId = getResources().getIdentifier("button_" + destinationMoveList.get(destinationMoveList.size() - 1), "id", getPackageName());
                ImageButton firstButton = ((ImageButton) findViewById(firstButtonId));

                int secondButtonID = getResources().getIdentifier("button_" + originMoveList.get(originMoveList.size() - 1), "id", getPackageName());
                ImageButton secondButton = ((ImageButton) findViewById(secondButtonID));


                Resources res = getResources();
                int movingPieceID = res.getIdentifier(pieceName.toLowerCase(), "drawable", getPackageName());
                PromotedPiece selectedPromotedPiece = null;
                for(PromotedPiece p : board.promotedPieces){
                    if(p.getMoveCount() == moveCount){
                        selectedPromotedPiece = p;
                        break;
                    }
                }
                if(selectedPromotedPiece != null) {

                    System.out.println("PROMOTION IN REPLAY");
                    ChessPiece promotedPiece = selectedPromotedPiece.getPromotedPiece();
                    System.out.println(promotedPiece.toString());

                    int dest[] = {Integer.parseInt(destinationMoveList.get(destinationMoveList.size() - 1).substring(0, 1)), Integer.parseInt((destinationMoveList.get(destinationMoveList.size() - 1).substring(1)))};

                    if (promotedPiece.getColor() == 'w') {
                        board.board[dest[0]][dest[1]] = new Pawn('w', dest[0], dest[1]);
                        movingPieceID = res.getIdentifier("wp", "drawable", getPackageName());
                    }else{
                        board.board[dest[0]][dest[1]] = new Pawn('b', dest[0], dest[1]);
                        movingPieceID = res.getIdentifier("bp", "drawable", getPackageName());
                    }



                    promote = true;



                }

                secondButton.setImageResource(movingPieceID);
                firstButton.setImageResource(android.R.color.transparent);

                board.movePiece(destinationMoveList.get(destinationMoveList.size() - 1), originMoveList.get(originMoveList.size() - 1));
                if(putBack) {
                    ChessPiece newPiece = board.table.get(moveCount);
                    System.out.println("putting back: " + newPiece);
                    board.table.remove(moveCount);
                    int dest[] = {Integer.parseInt(destinationMoveList.get(destinationMoveList.size() - 1).substring(0, 1)), Integer.parseInt((destinationMoveList.get(destinationMoveList.size() - 1).substring(1)))};

                    System.out.println(Integer.toString(dest[0]) + Integer.toString(dest[1]));
                    board.board[dest[0]][dest[1]] = newPiece;
                    board.printBoard(board.board);
                    int moveID = res.getIdentifier(newPiece.toString().toLowerCase(), "drawable", getPackageName());
                    firstButton.setImageResource(moveID);

                    putBack = false;
                }
                originMoveList.remove(originMoveList.size() - 1);
                destinationMoveList.remove(destinationMoveList.size() - 1);
                board.promotedPieces.remove(selectedPromotedPiece);
                undoDone = true;
                if (whitesTurn) {
                    whitesTurn = false;

                } else {
                    whitesTurn = true;

                }


            }
        });


        drawButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog dialog = new AlertDialog.Builder(NewGame.this).setTitle("Does the opponent also wish to draw?").setPositiveButton("DRAW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new AlertDialog.Builder(NewGame.this).setTitle("DRAW!").setItems(new CharSequence[]
                                        {"New Game", "Close Window", "Save Game"},
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // The 'which' argument contains the index position
                                        // of the selected item
                                        switch (which) {
                                            case 0:
                                                board = new Board();
                                                originMoveList.clear();
                                                destinationMoveList.clear();
                                                finish();
                                                startActivity(getIntent());
                                                break;
                                            case 1:
                                                Intent newGameIntent = new Intent(NewGame.this, MainActivity.class);
                                                startActivity(newGameIntent);
                                                break;
                                            case 2:
                                                draw = true;
                                                saveGame(v);
                                                saveGame = true;
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                }).show();
                    }
                }).setNegativeButton("Cancel", null).show();
            }
        });


        resignButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog dialog = new AlertDialog.Builder(NewGame.this).setTitle("Are you sure you want to resign?").setPositiveButton("RESIGN", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new AlertDialog.Builder(NewGame.this).setTitle("RESIGN!").setItems(new CharSequence[]
                                        {"New Game", "Close Window", "Save Game"},
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // The 'which' argument contains the index position
                                        // of the selected item
                                        switch (which) {
                                            case 0:
                                                board = new Board();
                                                originMoveList.clear();
                                                destinationMoveList.clear();
                                                finish();
                                                startActivity(getIntent());
                                                break;
                                            case 1:
                                                Intent newGameIntent = new Intent(NewGame.this, MainActivity.class);
                                                startActivity(newGameIntent);
                                                break;
                                            case 2:
                                                resign = true;
                                                saveGame(v);
                                                saveGame = true;
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                }).show();
                    }
                }).setNegativeButton("Cancel", null).show();
            }
        });


        for (int i = 0; i < layout0.getChildCount(); i++) {
            View v = layout0.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout1.getChildCount(); i++) {
            View v = layout1.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout2.getChildCount(); i++) {
            View v = layout2.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout3.getChildCount(); i++) {
            View v = layout3.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout4.getChildCount(); i++) {
            View v = layout4.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout5.getChildCount(); i++) {
            View v = layout5.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout6.getChildCount(); i++) {
            View v = layout6.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

        for (int i = 0; i < layout7.getChildCount(); i++) {
            View v = layout7.getChildAt(i);
            if (v instanceof ImageButton) {
                v.setOnClickListener(this);
            }
        }

    }

    @Override
    public void onClick(View view) {

        if (!firstClick && !secondClick) {

            firstTile = view.getResources().getResourceName(view.getId());
            firstTile = firstTile.substring(firstTile.length() - 2);
            int[] firstPieceLocation = Board.translateToArray(firstTile);
            ChessPiece firstPiece = Board.getPieceAtLocation(firstPieceLocation);
            if (firstPiece == null) {
                return;
            }
            if (firstPiece != null && whitesTurn && !(firstPiece.getColor() == 'w')) {
                return;

            } else if (firstPiece != null && !whitesTurn && !(firstPiece.getColor() == 'b')) {
                return;
            }
            firstClick = true;
            int firstButtonId = getResources().getIdentifier("button_" + firstTile, "id", getPackageName());
            ImageButton firstButton = ((ImageButton) findViewById(firstButtonId));
            firstButton.setBackgroundColor(Color.parseColor("#5df542"));

        }

        else if (firstClick && !secondClick) {

            secondClick = true;
            secondTile = view.getResources().getResourceName(view.getId());
            secondTile = secondTile.substring(secondTile.length() - 2);
            int secondButtonId;

            if (AIMoves != null && AIMoves.size() != 0) {
                if (AIMoves.get(1) < 10) {
                    secondButtonId = getResources().getIdentifier("button_0" + AIMoves.get(1), "id", getPackageName());
                } else {
                    secondButtonId = getResources().getIdentifier("button_" + AIMoves.get(1), "id", getPackageName());

                }
                ImageButton secondButton = ((ImageButton) findViewById(secondButtonId));
                int i, j;
                if (AIMoves.get(1) < 10) {
                    i = 0;
                    j = Integer.parseInt(Integer.toString(AIMoves.get(1)));
                } else {
                    i = Integer.parseInt(Integer.toString(AIMoves.get(1)).substring(0, 1));
                    j = Integer.parseInt(Integer.toString(AIMoves.get(1)).substring(1));
                }

                if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                    secondButton.setBackgroundColor(Color.parseColor("#DEB887"));
                } else {
                    secondButton.setBackgroundColor(Color.parseColor("#8B4513"));
                }

                int firstButtonId;
                if (AIMoves.get(0) < 10) {
                    firstButtonId = getResources().getIdentifier("button_0" + AIMoves.get(0), "id", getPackageName());
                } else {
                    firstButtonId = getResources().getIdentifier("button_" + AIMoves.get(0), "id", getPackageName());

                }

                ImageButton firstButton = ((ImageButton) findViewById(firstButtonId));
                if (AIMoves.get(0) < 10) {
                    i = 0;
                    j = Integer.parseInt(Integer.toString(AIMoves.get(0)));
                } else {
                    i = Integer.parseInt(Integer.toString(AIMoves.get(0)).substring(0, 1));
                    j = Integer.parseInt(Integer.toString(AIMoves.get(0)).substring(1));
                }

                if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                    firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                } else {
                    firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                }

            }

            if (AIMoves != null)
                AIMoves.clear();
        }

        if (firstClick && secondClick) {
            boolean validMove = Board.checkMoveValidity(Board.getPieceAtLocation(Board.translateToArray(firstTile)), secondTile);
            char color;
            ChessPiece movingPiece = Board.getPieceAtLocation(Board.translateToArray(firstTile));
            String pieceName = movingPiece.toString();

            int firstButtonId = getResources().getIdentifier("button_" + firstTile, "id", getPackageName());
            ImageButton firstButton = ((ImageButton) findViewById(firstButtonId));

            int secondButtonID = getResources().getIdentifier("button_" + secondTile, "id", getPackageName());
            ImageButton secondButton = ((ImageButton) findViewById(secondButtonID));




            if (validMove) {

                if (whitesTurn) {
                    color = 'b';
                } else {
                    color = 'w';
                }


                //PROMOTION WHITE
                if (secondTile.charAt(0) == '0' && (Board.getPieceAtLocation(Board.translateToArray(firstTile))) instanceof Pawn && Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor() == 'w') {

                    final Dialog dialog = new Dialog(this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.white_promotion_dialogue);

                    ImageButton wbButton = dialog.findViewById(R.id.wbBut);
                    ImageButton wqButton = dialog.findViewById(R.id.wqBut);
                    ImageButton wrButton = dialog.findViewById(R.id.wrBut);
                    ImageButton wnButton = dialog.findViewById(R.id.wnBut);

                    StringBuilder sb = new StringBuilder();

                    wbButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "B", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.wb);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    builder.setTitle("Checkmate! White wins");
                                    checkmateWhite = true;
                                } else {
                                    builder.setTitle("Checkmate! Black wins");
                                    checkmateBlack = true;
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:

                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();

                                                }
                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        int i = Integer.parseInt(firstTile.substring(0, 1));
                        int j = Integer.parseInt(firstTile.substring(1));
                        if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                            firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                        } else {
                            firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                        }
                        dialog.dismiss();
                    });

                    wqButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "Q", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.wq);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    builder.setTitle("CheckMate! White wins");
                                    checkmateWhite = true;
                                } else {
                                    builder.setTitle("CheckMate! Black wins");
                                    checkmateBlack = true;
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:
                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();

                                                }
                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        int i = Integer.parseInt(firstTile.substring(0, 1));
                        int j = Integer.parseInt(firstTile.substring(1));
                        if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                            firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                        } else {
                            firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                        }
                        dialog.dismiss();
                    });

                    wrButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "R", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.wr);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    builder.setTitle("CheckMate! White wins");
                                    checkmateWhite = true;
                                } else {
                                    builder.setTitle("CheckMate! Black wins");
                                    checkmateBlack = true;
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:
                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();

                                                }

                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        int i = Integer.parseInt(firstTile.substring(0, 1));
                        int j = Integer.parseInt(firstTile.substring(1));
                        if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                            firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                        } else {
                            firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                        }
                        dialog.dismiss();
                    });

                    wnButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "N", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.wn);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    builder.setTitle("CheckMate! White wins");
                                    checkmateWhite = true;
                                } else {
                                    checkmateBlack = true;
                                    builder.setTitle("CheckMate! Black wins");
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:
                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();

                                                }

                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        int i = Integer.parseInt(firstTile.substring(0, 1));
                        int j = Integer.parseInt(firstTile.substring(1));
                        if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                            firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                        } else {
                            firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                        }
                        dialog.dismiss();
                    });
                    dialog.show();

                    undoDone = false;
                    moveCount++;

                    int i = Integer.parseInt(firstTile.substring(0, 1));
                    int j = Integer.parseInt(firstTile.substring(1));
                    if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                        firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                    } else {
                        firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                    }

                    originMoveList.add(firstTile);
                    destinationMoveList.add(secondTile);
                }


                //BLACK PROMOTION
                else if (secondTile.charAt(0) == '7' && (Board.getPieceAtLocation(Board.translateToArray(firstTile))) instanceof Pawn && Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor() == 'b') {
                    final Dialog dialog = new Dialog(this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.black_promotion_dialouge);

                    ImageButton bbButton = dialog.findViewById(R.id.bbBut);
                    ImageButton bqButton = dialog.findViewById(R.id.bqBut);
                    ImageButton brButton = dialog.findViewById(R.id.brBut);
                    ImageButton bnButton = dialog.findViewById(R.id.bnBut);

                    bbButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "B", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.bb);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    checkmateWhite = true;
                                    builder.setTitle("CheckMate! White wins");
                                } else {
                                    builder.setTitle("CheckMate! Black wins");
                                    checkmateBlack = true;
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:
                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();
                                                }

                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        dialog.dismiss();
                    });

                    bqButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "Q", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.bq);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    checkmateWhite = true;
                                    builder.setTitle("CheckMate! White wins");
                                } else {
                                    checkmateBlack = true;
                                    builder.setTitle("CheckMate! Black wins");
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:
                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();

                                                }

                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        dialog.dismiss();
                    });

                    brButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "R", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.br);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    checkmateWhite = true;
                                    builder.setTitle("CheckMate! White wins");
                                } else {
                                    checkmateBlack = true;
                                    builder.setTitle("CheckMate! Black wins");
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:

                                                }

                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        dialog.dismiss();
                    });

                    bnButton.setOnClickListener(v -> {
                        boolean p = board.checkPromotion(firstTile, secondTile, "N", Board.getPieceAtLocation(Board.translateToArray(firstTile)).getColor());
                        secondButton.setImageResource(R.drawable.bn);
                        firstButton.setImageResource(android.R.color.transparent);
                        if (Board.isKingChecked(color, Board.board)) {
                            char color2;
                            if (whitesTurn)
                                color2 = 'w';
                            else
                                color2 = 'b';

                            if (Board.isKingCheckMated(color2)) {


                                Context context = this;
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                if (color2 == 'w') {
                                    checkmateWhite = true;
                                    builder.setTitle("CheckMate! White wins");
                                } else {
                                    checkmateBlack = true;
                                    builder.setTitle("CheckMate! Black wins");
                                }

                                builder.setItems(new CharSequence[]
                                                {"New Game", "Close Window", "Save Game"},
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // The 'which' argument contains the index position
                                                // of the selected item
                                                switch (which) {
                                                    case 0:
                                                        board = new Board();
                                                        originMoveList.clear();
                                                        destinationMoveList.clear();
                                                        finish();
                                                        startActivity(getIntent());
                                                        break;
                                                    case 1:
                                                        Intent newGameIntent = new Intent(context, MainActivity.class);
                                                        startActivity(newGameIntent);
                                                        break;
                                                    case 2:
                                                        saveGame(view);
                                                        saveGame = true;
                                                        dialog.dismiss();
                                                }
                                            }
                                        });
                                builder.create().show();


                            } else {
                                System.out.println("Check");
                            }
                            System.out.println();

                        }

                        dialog.dismiss();
                    });

                    dialog.show();

                    undoDone = false;
                    moveCount++;

                    int i = Integer.parseInt(firstTile.substring(0, 1));
                    int j = Integer.parseInt(firstTile.substring(1));
                    if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                        firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                    } else {
                        firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                    }
                    originMoveList.add(firstTile);
                    destinationMoveList.add(secondTile);
                }

                else{

                    originMoveList.add(firstTile);
                    destinationMoveList.add(secondTile);
                    board.movePiece(firstTile, secondTile);
                    undoDone = false;
                    moveCount++;

                    Resources res = getResources();
                    int movingPieceID = res.getIdentifier(pieceName.toLowerCase(), "drawable", getPackageName());

                    secondButton.setImageResource(movingPieceID);
                    firstButton.setImageResource(android.R.color.transparent);

                    if (Board.printCastle == true) {

                        int rookOriginID = getResources().getIdentifier("button_" + Board.rookinit, "id", getPackageName());
                        ImageButton rookOriginButton = ((ImageButton) findViewById(rookOriginID));

                        int rookDestinationID = getResources().getIdentifier("button_" + Board.rookLocation, "id", getPackageName());
                        ImageButton rookDestinationButton = ((ImageButton) findViewById(rookDestinationID));

                        rookOriginButton.setImageResource(android.R.color.transparent);
                        if (whitesTurn)
                            rookDestinationButton.setImageResource(R.drawable.wr);
                        else
                            rookDestinationButton.setImageResource(R.drawable.br);

                        Board.printCastle = false;
                    }

                    if (Board.enpass) {
                        if (whitesTurn) {
                            int result = Integer.parseInt(secondTile);
                            result = result + 10;
                            String tilenum = Integer.toString(result);
                            int enpassantID = getResources().getIdentifier("button_" + tilenum, "id", getPackageName());
                            ImageButton enpassbutton = ((ImageButton) findViewById(enpassantID));
                            enpassbutton.setImageResource(android.R.color.transparent);


                        } else {
                            int result = Integer.parseInt(secondTile);
                            result = result - 10;
                            String tilenum = Integer.toString(result);
                            int enpassantID = getResources().getIdentifier("button_" + tilenum, "id", getPackageName());
                            ImageButton enpassbutton = ((ImageButton) findViewById(enpassantID));
                            enpassbutton.setImageResource(android.R.color.transparent);
                        }

                        Board.enpass = false;
                    }

                    if (Board.isKingChecked(color, Board.board)) {
                        char color2;
                        if (whitesTurn)
                            color2 = 'w';
                        else
                            color2 = 'b';

                        if (Board.isKingCheckMated(color2)) {


                            Context context = this;
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            if (color2 == 'w') {
                                builder.setTitle("CheckMate! White wins");
                                checkmateWhite = true;
                            } else {
                                builder.setTitle("CheckMate! Black wins");
                                checkmateBlack = true;
                            }

                            builder.setItems(new CharSequence[]
                                            {"New Game", "Close Window", "Save Game"},
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // The 'which' argument contains the index position
                                            // of the selected item
                                            switch (which) {
                                                case 0:
                                                    board = new Board();
                                                    originMoveList.clear();
                                                    destinationMoveList.clear();
                                                    finish();
                                                    startActivity(getIntent());
                                                    break;
                                                case 1:
                                                    Intent newGameIntent = new Intent(context, MainActivity.class);
                                                    startActivity(newGameIntent);
                                                    break;
                                                case 2:
                                                    saveGame(view);
                                                    saveGame = true;
                                                    dialog.dismiss();
                                            }
                                        }
                                    });
                            builder.create().show();


                        } else {
                            System.out.println("Check");
                        }
                        System.out.println();
                    }

                    int i = Integer.parseInt(firstTile.substring(0, 1));
                    int j = Integer.parseInt(firstTile.substring(1));
                    if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                        firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                    } else {
                        firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                    }

                }

                if (whitesTurn) {
                    whitesTurn = false;
                } else {
                    whitesTurn = true;
                }
            }
            else{

                int i = Integer.parseInt(firstTile.substring(0, 1));
                int j = Integer.parseInt(firstTile.substring(1));
                if (((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                    firstButton.setBackgroundColor(Color.parseColor("#DEB887"));
                } else {
                    firstButton.setBackgroundColor(Color.parseColor("#8B4513"));
                }

            }

            firstClick = false;
            secondClick = false;


        }
    }


    public void saveGame(View view) {


        Context c = this;
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        AlertDialog dialog = new AlertDialog.Builder(c).setView(input).setTitle("Enter Game Title").setPositiveButton("OK", null).setNegativeButton("Cancel", null).create();


        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface){
                Button button = ((AlertDialog) dialog).getButton((AlertDialog.BUTTON_POSITIVE));
                button.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View view){
                        ArrayList<String> gameListStrings = new ArrayList<>();
                        for(Game g : MainActivity.gameList){
                            gameListStrings.add(g.getTitle());
                        }
                        if(!gameListStrings.contains(input.getText().toString())) {

                            String endType;
                            if(draw){
                                MainActivity.gameList.add(new Game(input.getText().toString(), originMoveList, destinationMoveList, LocalDate.now(), board.promotedPieces, "draw"));
                            }
                            else if(resign)
                                MainActivity.gameList.add(new Game(input.getText().toString(), originMoveList, destinationMoveList, LocalDate.now(), board.promotedPieces, "resign"));
                            else if(checkmateWhite) MainActivity.gameList.add(new Game(input.getText().toString(), originMoveList, destinationMoveList, LocalDate.now(), board.promotedPieces, "checkmateWhite"));
                            else if(checkmateBlack) MainActivity.gameList.add(new Game(input.getText().toString(), originMoveList, destinationMoveList, LocalDate.now(), board.promotedPieces, "checkmateBlack"));


                            System.out.println("ADDED TO GAMELIST");

                            try {
                                MainActivity.writeGame();
                                System.out.println("WROtE GAME");
                                dialog.dismiss();

                            } catch (IOException io) {
                                io.printStackTrace();
                            }
                        }
                        else{
                            Toast.makeText(c,"Game title already exists! Please enter a different title.", Toast.LENGTH_SHORT);
                        }


                    }
                });
            }

        });


        dialog.show();

    }
}